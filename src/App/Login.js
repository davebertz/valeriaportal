import React from 'react';
import Keycloak from 'keycloak-js';
import Routes from './Routes.js';
import './components/styles/general.css';

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      keycloak: null,
      authenticated: false
    };
  }

  componentDidMount() {
    const keycloak = Keycloak({
        url: 'https://keycloak-valeriademo.svd-pca.svc.ulaval.ca/auth',
        realm: 'valeria',
        clientId: 'portal'
    });
    keycloak.init({onLoad: 'login-required'}).then(authenticated => {
        this.setState({ keycloak: keycloak, authenticated: authenticated })
    });
  }

  render() {
    //GRANT ROUTES IF LOGGED IN
    if(this.state.keycloak) {
      if(this.state.authenticated) return (
        <Routes keycloak={this.state.keycloak}/>
      ); else return (<div>Unable to authenticate!</div>)
    }
    return (
      <div>Loading, please wait...</div>
    );
  }
}

export { Login };