import React, { Component, Suspense } from 'react';
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
import ApplicationsPage from './components/Pages/ApplicationsPage.js';
import ExplorerPage from './components/Pages/ExplorerPage.js';
import HomePage from './components/Pages/HomePage.js';
import KeysPage from './components/Pages/KeysPage';
import HelpPage from './components/Pages/HelpPage';
import Header from './components/Nav/Header.js';
import SideNav from './components/Nav/SideNav';
import StorageGraphsPage from './components/Pages/StorageGraphsPage.js';

class Routes extends Component {

    constructor(props){
        super(props);
        this.state = {
            isNavOpen : true
        }
        this.onNavToggle = this.onNavToggle.bind(this);
    }

    onNavToggle(){
        this.setState({
            isNavOpen: !this.state.isNavOpen
        });
    };

    render() { 
        const HeaderNav = <Header keycloak={this.props.keycloak}/>;
        const Sidebar = <SideNav/>;
        return ( 
            <Suspense fallback='loading'>
                <BrowserRouter>
                    <Switch>
                        <Route exact path="/" render={() => <Redirect to="/home"/>}
                        />
                        <Route exact path="/home" render={() => <HomePage header={HeaderNav} sidebar={Sidebar}/>}
                        />
                        <Route exact path="/applications" render={() => <ApplicationsPage header={HeaderNav} sidebar={Sidebar}/>}
                        />
                        <Route exact path="/s3explorer" render={() => <ExplorerPage header={HeaderNav} sidebar={Sidebar} keycloak={this.props.keycloak}/>}
                        />
                        <Route exact path="/storage/graphs" render={() => <StorageGraphsPage header={HeaderNav} sidebar={Sidebar}/>}
                        />
                        <Route exact path="/storage/keys" render={() => <KeysPage header={HeaderNav} sidebar={Sidebar} keycloak={this.props.keycloak}/>}
                        />
                        <Route exact path="/help" render={() => <HelpPage header={HeaderNav} sidebar={Sidebar}/>}
                        />
                    </Switch>
                </BrowserRouter>
            </Suspense>

        );
    }
}
export default Routes;