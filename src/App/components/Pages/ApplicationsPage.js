import React from 'react';
import {
    Breadcrumb,
    BreadcrumbItem,
    Button,
    Card,
    CardBody,
    Page,
    PageSection,
    PageSectionVariants,
    TextContent,
    Text,
    CardHeader,
    CardFooter
    } from '@patternfly/react-core';
import '../styles/app-page.css';
import { Translation } from 'react-i18next';

class ApplicationsPage extends React.Component {

    render() { 

        const PageBreadcrumb = (
            <Breadcrumb>
                <BreadcrumbItem to="home"><Translation>{(t) => <span>{t('home')}</span>}</Translation></BreadcrumbItem>
                <BreadcrumbItem isActive>
                    <Translation>{(t) => <span>{t('applications')}</span>}</Translation>
                </BreadcrumbItem>
            </Breadcrumb>
        );
        return ( 
            <React.Fragment>
                <Page header={this.props.header} sidebar={this.props.sidebar} breadcrumb={PageBreadcrumb} isManagedSidebar>
                    <PageSection variant={PageSectionVariants.light} isFilled>
                        <TextContent>
                            <Text component="h1"><Translation>{(t) => <span>{t('applications')}</span>}</Translation></Text>
                        </TextContent>
                            <Card>
                                <CardHeader>
                                    Jupyter
                                </CardHeader>
                                <CardBody>
                                    <img src='https://portal-valeriademo.svd-pca.svc.ulaval.ca/assets/img/Jupyter_logo.svg' className='logo' alt='jupyter-logo'/> <br/>
                                    <Translation>{(t) => <span>{t('jupyterDesc')}</span>}</Translation>
                                </CardBody>
                                <CardFooter>
                                    <Button className='app-btns' variant="secondary" component='a' href='https://jupyterhub-valeriademo.svd-pca.svc.ulaval.ca/' target='_blank'><Translation>{(t) => <span>{t('jupyterBtn')}</span>}</Translation></Button>
                                </CardFooter>
                            </Card>
                            <Card>
                                <CardHeader>
                                    CKAN
                                </CardHeader>
                                <CardBody>
                                    <img src='https://portal-valeriademo.svd-pca.svc.ulaval.ca/assets/img/ckan.png' className='logo' alt='ckan-logo'/> <br/>
                                    <Translation>{(t) => <span>{t('ckanDesc')}</span>}</Translation>
                                </CardBody>
                                <CardFooter>
                                    <Button className='app-btns' variant="secondary" component='a' href='http://ul-svd-ex-cka01.ul.ca/' target='_blank'><Translation>{(t) => <span>{t('ckanBtn')}</span>}</Translation></Button>
                                </CardFooter>
                            </Card>
                            <Card>
                                <CardHeader>
                                    GitLab
                                </CardHeader>
                                <CardBody>
                                    <img src='https://portal-valeriademo.svd-pca.svc.ulaval.ca/assets/img/GitLab_Logo.svg' className='logo' alt='gitlab-logo'/> <br/>
                                    <Translation>{(t) => <span>{t('gitDesc')}</span>}</Translation>
                                </CardBody>
                                <CardFooter>
                                    <Button className='app-btns' variant="secondary" component='a' href='https://gitlab-valeriademo.svd-pca.svc.ulaval.ca/' target='_blank'><Translation>{(t) => <span>{t('gitBtn')}</span>}</Translation></Button>
                                </CardFooter>
                            </Card>
                    </PageSection>
                </Page>
            </React.Fragment>            
        );
    }
} 
export default ApplicationsPage;