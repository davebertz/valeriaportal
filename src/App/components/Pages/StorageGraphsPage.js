import React, { Component } from 'react';
import { Page, PageSection, PageSectionVariants } from '@patternfly/react-core';
import StkDonut from '../utils/StkDonut';
//import StkChartPie from '../utils/StkChartPie';
import '../styles/StorageGraphsPage.css';

class StorageGraphsPage extends Component {
    render() { 
        return ( 
            <React.Fragment>
                <Page header={this.props.header} sidebar={this.props.sidebar} isManagedSidebar>
                    <PageSection variant={PageSectionVariants.light}>
                        <StkDonut/>
                    </PageSection>
                </Page>
            </React.Fragment>
        );
    }
}
export default StorageGraphsPage;
