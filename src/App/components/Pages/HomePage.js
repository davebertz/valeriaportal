import React from 'react';
import {
    Page,
    PageSection,
    PageSectionVariants,
    } from '@patternfly/react-core';
import '../styles/home-page.css';
import {ReactComponent as ValeriaImgEN} from '../img/VALERIA_ang_coul.svg';
import {ReactComponent as ValeriaImgFR} from '../img/VALERIA_fr_coul.svg';
import i18next from 'i18next';

class HomePage extends React.Component {

    render() {
        if(i18next.language === 'fr')
            return (
                <React.Fragment>
                    <Page header={this.props.header} sidebar={this.props.sidebar} isManagedSidebar>
                        <PageSection variant={PageSectionVariants.light}>
                            <ValeriaImgFR id='valeria-signature' alt='valeria-signature'/>
                        </PageSection>
                    </Page>
                </React.Fragment> 
            );else return (
                <React.Fragment>
                    <Page header={this.props.header} sidebar={this.props.sidebar} isManagedSidebar>
                        <PageSection variant={PageSectionVariants.light}>
                            <ValeriaImgEN id='valeria-signature' alt='valeria-signature'/>
                        </PageSection>
                    </Page>
                </React.Fragment>   
            );
        }
    } 
export default HomePage;