import React from 'react';
import {
    Breadcrumb,
    BreadcrumbItem,
    Page,
    PageSection,
    PageSectionVariants,
    TextContent,
    Text,
} from '@patternfly/react-core';
import { Translation } from 'react-i18next';
import GetKeys from '../utils/GetKeys';

class KeysPage extends React.Component {
    render() { 
        const PageBreadcrumb = (
            <Breadcrumb>
                <BreadcrumbItem to="/home"><Translation>{(t) => <span>{t('home')}</span>}</Translation></BreadcrumbItem>
                <BreadcrumbItem isActive>
                    <Translation>{(t) => <span>{t('showKeys')}</span>}</Translation>
                </BreadcrumbItem>
            </Breadcrumb>
        );
        return ( 
            <React.Fragment>
                <Page header={this.props.header} sidebar={this.props.sidebar} breadcrumb={PageBreadcrumb} isManagedSidebar>
                    <PageSection variant={PageSectionVariants.light}>
                        <TextContent>
                            <Text component="h1"><Translation>{(t) => <span>{t('showKeys')}</span>}</Translation></Text>
                            <GetKeys keycloak={this.props.keycloak}/>
                        </TextContent>
                    </PageSection>
                </Page>
            </React.Fragment>            
        );
    }
} 
export default KeysPage;

