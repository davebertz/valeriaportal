import React from 'react';
import {
    Breadcrumb,
    BreadcrumbItem,
    Page,
    PageSection,
    PageSectionVariants,
    Form,
    FormGroup,
    TextInput,
    TextArea,
    Button,
    ActionGroup
} from '@patternfly/react-core';
import { Translation } from 'react-i18next';

class HelpPage extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            objValue: '',
            desValue: ''
        }
        this.handleTextInputChangeObj = objValue => {
            this.setState({ objValue });
        };
        this.handleTextInputChangeDes = desValue => {
            this.setState({ desValue });
        };
    }
    
    render() { 
        const PageBreadcrumb = (
            <Breadcrumb>
                <BreadcrumbItem to="/home"><Translation>{(t) => <span>{t('home')}</span>}</Translation></BreadcrumbItem>
                <BreadcrumbItem isActive>
                    <Translation>{(t) => <span>{t('help')}</span>}</Translation>
                </BreadcrumbItem>
            </Breadcrumb>
        );
        return ( 
            <React.Fragment>
                <Page header={this.props.header} sidebar={this.props.sidebar} breadcrumb={PageBreadcrumb} isManagedSidebar>
                    <PageSection variant={PageSectionVariants.light}>
                        <Form>
                            <FormGroup
                            label="Objet"
                            isRequired
                            fieldId="object"
                            >
                                <TextInput
                                    isRequired
                                    type="text"
                                    id="object"
                                    value={this.state.objValue}
                                    onChange={this.handleTextInputChangeObj}
                                />
                            </FormGroup>
                            <FormGroup label="Description" isRequired fieldId="description">
                                <TextArea
                                    isRequired
                                    id="description"
                                    value={this.state.desValue}
                                    onChange={this.handleTextInputChangeDes}
                                />
                            </FormGroup>
                            <ActionGroup>
                                <Button variant="primary">Submit</Button>
                            </ActionGroup>
                        </Form>
                    </PageSection>
                </Page>
            </React.Fragment>            
        );
    }
} 
export default HelpPage;

