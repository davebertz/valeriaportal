import React from 'react';
import {
    Breadcrumb,
    BreadcrumbItem,
    Page,
    PageSection,
    PageSectionVariants,
    TextContent,
    Text,
    } from '@patternfly/react-core';
    import { Translation } from 'react-i18next';
import GetBuckets from '../utils/GetBuckets';

class ExplorerPage extends React.Component {
    render() { 
        const PageBreadcrumb = (
            <Breadcrumb>
                <BreadcrumbItem to="home"><Translation>{(t) => <span>{t('home')}</span>}</Translation></BreadcrumbItem>
                <BreadcrumbItem isActive>
                    <Translation>{(t) => <span>{t('s3explorer')}</span>}</Translation>
                </BreadcrumbItem>
            </Breadcrumb>
        );
        return ( 
            <React.Fragment>
                <Page header={this.props.header} sidebar={this.props.sidebar} breadcrumb={PageBreadcrumb} isManagedSidebar>
                    <PageSection variant={PageSectionVariants.light}>
                        <TextContent>
                            <Text component="h1"><Translation>{(t) => <span>{t('s3explorer')}</span>}</Translation></Text>
                            <Text component='h2' id='content'></Text>
                            <GetBuckets keycloak={this.props.keycloak}/>
                        </TextContent>
                    </PageSection>
                </Page>
            </React.Fragment>            
        );
    }
} 
export default ExplorerPage;

