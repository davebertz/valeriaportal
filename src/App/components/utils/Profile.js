import React, { Component } from 'react';
import {
    DropdownItem
} from '@patternfly/react-core';
import '../styles/general.css';
import i18next from "i18next";

class Profile extends Component {
    render() { 
        if(i18next.language === 'fr') return ( 
            <DropdownItem key={this.props.key} component="a" href='https://keycloak-valeriademo.svd-pca.svc.ulaval.ca/auth/realms/valeria/account/' target='_blank'>
                Mon profil
            </DropdownItem>
        ); else return (
            <DropdownItem key={this.props.key} component="a" href='https://keycloak-valeriademo.svd-pca.svc.ulaval.ca/auth/realms/valeria/account/' target='_blank'>
                My profile
            </DropdownItem>
        );
    }
}
export default Profile;