import React, { Component } from 'react';
class GetKeys extends Component {
    constructor(props){
        super(props);
        this.state = {
            vaultToken: "",
            vaultId: "",
            awsClientId: "",
            awsSecretKey: ""
        };
    }
    componentDidMount(){
        const that = this; //this est undefined si utilisé dans les fetch
        var keycloak = this.props.keycloak;
        var loadData = function () {
            fetch('https://vault-vault.svd-pca.svc.ulaval.ca/v1/auth/jwt/login', {
                    method: 'POST', 
                    mode: 'cors', 
                    cache: 'no-cache',
                    credentials: 'same-origin',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    redirect: 'follow', 
                    referrer: 'no-referrer', 
                    body: JSON.stringify({"role":null, "jwt": keycloak.token})
            })
            .then(response => response.json())
            .then(function(data) {
                var vault_token = data.auth.client_token;
                var vault_id = data.auth.entity_id;
                that.setState({ 
                    vaultToken: vault_token,
                    vaultId: vault_id
                });
        
                fetch('https://vault-vault.svd-pca.svc.ulaval.ca/v1/valeria/data/users/' + vault_id + '/ceph', {
                    headers: {
                        'X-Vault-Token': vault_token,
                    },
                    method: 'POST',
                    body: JSON.stringify({"data":{"aws_client_id":create_UUID(),"aws_secret_key":create_UUID()}})
                })
                .catch(error => console.error(error));
        
                fetch('https://vault-vault.svd-pca.svc.ulaval.ca/v1/valeria/data/users/' + vault_id + '/ceph', {
                    headers: {
                        'X-Vault-Token': vault_token,
                    },
                    method: 'GET',
                })
                .then(response => response.json())
                .then(function(data) {
                    var aws_client_id = data.data.data.aws_client_id;
                    var aws_secret_key = data.data.data.aws_secret_key;
                    that.setState({ 
                        awsClientId: aws_client_id,
                        awsSecretKey: aws_secret_key
                    });
                })
                .catch(error => console.error(error));
            })
            .catch(error => console.error(error));
        };
        
        var create_UUID = function(){
            var dt = new Date().getTime();
            var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
                var r = (dt + Math.random()*16)%16 | 0;
                dt = Math.floor(dt/16);
                return (c==='x' ? r :(r&0x3|0x8)).toString(16);
            });
            return uuid;
        }
        loadData()
    }
    render() { 
        return ( 
            <React.Fragment>
                <div>Vault ID = {this.state.vaultId}</div>
                <div>Vault Token = {this.state.vaultToken}</div>
                <div>AWS Client ID = {this.state.awsClientId}</div>
                <div>AWS Secret Key = {this.state.awsSecretKey}</div>
                <div>Your token = {this.props.keycloak.token}</div>
            </React.Fragment>
        );
    }
}
export default GetKeys;