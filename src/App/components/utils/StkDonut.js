import React, { Component } from 'react';
import { ChartDonutUtilization } from '@patternfly/react-charts';

class StkDonut extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            spacer: '',
            used: 20
        }
    }
    render() { 
        const { spacer, used } = this.state;
        return ( 
            <div>
                <div className="donut-utilization-chart-legend-right">
                    <ChartDonutUtilization
                        data={{ x: 'GBps capacity', y: used }}
                        labels={datum => datum.x ? `${datum.x}: ${datum.y}%` : null}
                        legendData={[{ name: `Storage capacity: ${spacer}${used}%` }, { name: 'Unused' }]}
                        legendOrientation="vertical"
                        subTitle="of 100 GBps"
                        title={`${used}%`}
                        thresholds={[{ value: 60 }, { value: 90 }]}
                        width={435}
                    />
                </div>
            </div>
        );
    }
}
export default StkDonut;

