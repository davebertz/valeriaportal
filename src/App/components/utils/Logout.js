import React, { Component } from 'react';
import { DropdownItem } from '@patternfly/react-core';
import i18next from "i18next";

class Logout extends Component {

    logout() {
        this.props.keycloak.logout();
    }

    render() {
        if(i18next.language === 'fr') return (
            <DropdownItem key={this.props.key} component='button' onClick={ () => this.logout() }>
                Se déconnecter
            </DropdownItem>
        ); else return (
            <DropdownItem key={this.props.key} component='button' onClick={ () => this.logout() }>
                Logout
            </DropdownItem>
        );
    }
}
export default Logout;