import React, { Component } from 'react';

class UserDetails extends Component {

    constructor(props) {
        super(props);
        this.state = {
        name: ""
        };
        this.props.keycloak.loadUserInfo().then(userInfo => {
            this.setState({name: userInfo.name})
        });
    }

    render() {
        return (
            <i>{this.state.name}</i>
        );
    }
}

export default UserDetails;