import React, { Component } from 'react';
import { ChartLegend, ChartPie } from '@patternfly/react-charts';

class StkChartPie extends Component {
    render() { 
        return ( 
            <div>
                <div className="pie-chart-inline">
                    <div className="pie-chart-container">
                    <ChartPie
                        data={[{ x: 'Idle', y: 35 }, { x: 'In Use', y: 55 }, { x: 'Available', y: 10 }]}
                        labels={datum => `${datum.x}: ${datum.y}`}
                    />
                    </div>
                    <ChartLegend
                    data={[{ name: 'Idle' }, { name: 'In Use' }, { name: 'Available' }]}
                    height={230}
                    orientation={'horizontal'}
                    responsive={false}
                    x={70}
                    />
                </div>
            </div>
        );
    }
}
export default StkChartPie;
