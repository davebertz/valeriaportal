import React, { Component } from 'react';
import {ReactComponent as BrandImg} from '../img/VALERIA_Texte_Blanc.svg';
import {ReactComponent as ValeriaLogo} from '../img/VALERIA_Symbole_Blanc.svg';
import UserDetails from '../utils/UserDetails.js';
import Logout from '../utils/Logout.js';
import Profile from '../utils/Profile.js';
import {
    Dropdown,
    DropdownToggle,
    DropdownItem,
    PageHeader,
    Toolbar,
    ToolbarGroup,
    ToolbarItem
} from '@patternfly/react-core';
import '../styles/general.css';
import i18n from "i18next";
import { Translation } from 'react-i18next';

class Header extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            isDropdownOpen: false,
            isLangOpen: false
        }
    }

    onDropdownToggle = isDropdownOpen => {
        this.setState({
            isDropdownOpen
        });
    };
    
    onDropdownSelect = event => {
        this.setState({
            isDropdownOpen: !this.state.isDropdownOpen
        });
    };

    onLangToggle = isLangOpen => {
        this.setState({
            isLangOpen
        });
    };
    
    onLangSelect = event => {
        this.setState({
            isLangOpen: !this.state.isLangOpen
        });
    };

    changeLanguage = lng => {
        i18n.changeLanguage(lng);
        window.location.reload();
    };

    render() {
        const userDropdownItems = [
            <Profile key={0}/>,
            <Logout keycloak={this.props.keycloak} key={1}/> //Returns DropdownItem button named Logout
        ];
        const langDropdownItems = [
            <DropdownItem key={0} component="button" onClick={() => this.changeLanguage('fr')}>
                Français
            </DropdownItem>,
            <DropdownItem key={1} component="button" onClick={() => this.changeLanguage('en')}>
                English
            </DropdownItem>,
        ];
        const HeaderBigLogo = (
            <BrandImg id='header-logo' alt='header-logo'/>
        );
        const HeaderSmallLogo = (
            <ValeriaLogo id='valeria-logo' alt='valeria-logo'/>
        );

        const PageToolbar = (
            <Toolbar>
                <ToolbarGroup>
                    <ToolbarItem>
                        <Dropdown
                        isPlain
                        position="right"
                        onSelect={this.onDropdownSelect}
                        isOpen={this.state.isDropdownOpen}
                        toggle={<DropdownToggle onToggle={this.onDropdownToggle}><i className="pf-icon pf-icon-user"></i><UserDetails keycloak={this.props.keycloak}/></DropdownToggle>}
                        dropdownItems={userDropdownItems}
                        />
                    </ToolbarItem>
                </ToolbarGroup>
                <ToolbarGroup>
                    <ToolbarItem>
                        <Dropdown
                        isPlain
                        position="right"
                        onSelect={this.onLangSelect}
                        isOpen={this.state.isLangOpen}
                        toggle={<DropdownToggle onToggle={this.onLangToggle}>
                            <Translation>{(t) => <span>{t('lang')}</span>}</Translation>
                        </DropdownToggle>}
                        dropdownItems={langDropdownItems}
                        />
                    </ToolbarItem>
                </ToolbarGroup>
            </Toolbar>
        );
        if (window.innerWidth < 768) return (
            <PageHeader logo={HeaderSmallLogo} toolbar={PageToolbar} showNavToggle/>
        ); else return (
            <PageHeader logo={HeaderBigLogo} toolbar={PageToolbar} showNavToggle/>
        );
    }
}
export default Header;