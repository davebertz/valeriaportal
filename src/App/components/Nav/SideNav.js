import React, { Component } from 'react';
import {
    Nav,
    NavItem,
    NavList,
    NavExpandable,
    NavVariants,
    PageSidebar,
} from '@patternfly/react-core';
import { NavLink } from "react-router-dom";
import '../styles/general.css';
import { Translation } from 'react-i18next';
import i18next from "i18next";

class SideNav extends Component {
    render() { 
        const PageNavFR = (
            <Nav aria-label="Nav">
                <NavList variant={NavVariants.simple}>
                <NavItem>
                    <NavLink to="/applications"><i className="pf-icon pf-icon-applications"></i>
                    <Translation>{(t) => <span>{t('applications')}</span>}</Translation>
                    </NavLink>
                </NavItem>
                <NavItem>
                    <NavLink to="/s3explorer">
                        <i className="pf-icon pf-icon-storage-domain"></i>
                        <Translation>{(t) => <span>{t('s3explorer')}</span>}</Translation>
                    </NavLink>
                </NavItem>
                <NavExpandable title="Stockage" isExpanded>
                    <NavItem>
                        <NavLink to="/storage/graphs"><i className="pf-icon pf-icon-monitoring"></i><Translation>{(t) => <span>{t('graph')}</span>}</Translation></NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink to="/storage/keys"><i className="pf-icon pf-icon-key"></i><Translation>{(t) => <span>{t('showKeys')}</span>}</Translation></NavLink>
                    </NavItem>
                </NavExpandable>
                <NavItem>
                    <NavLink to="/help"><i className="pf-icon pf-icon-info"></i>
                        <Translation>{(t) => <span>{t('help')}</span>}</Translation>
                    </NavLink>
                </NavItem>
                </NavList>
            </Nav>
        );        
        const PageNavEN = (
        <Nav aria-label="Nav">
            <NavList variant={NavVariants.simple}>
            <NavItem>
                <NavLink to="/applications"><i className="pf-icon pf-icon-applications"></i>
                <Translation>{(t) => <span>{t('applications')}</span>}</Translation>
                </NavLink>
            </NavItem>
            <NavItem>
                <NavLink to="/s3explorer">
                    <i className="pf-icon pf-icon-storage-domain"></i>
                    <Translation>{(t) => <span>{t('s3explorer')}</span>}</Translation>
                </NavLink>
            </NavItem>
            <NavExpandable title='Storage' isExpanded>
                <NavItem>
                    <NavLink to="/storage/graphs"><i className="pf-icon pf-icon-monitoring"></i><Translation>{(t) => <span>{t('graph')}</span>}</Translation></NavLink>
                </NavItem>
                <NavItem>
                    <NavLink to="/storage/keys"><i className="pf-icon pf-icon-key"></i><Translation>{(t) => <span>{t('showKeys')}</span>}</Translation></NavLink>
                </NavItem>
            </NavExpandable>
            <NavItem>
                <NavLink to="/help"><i className="pf-icon pf-icon-info"></i>
                    <Translation>{(t) => <span>{t('help')}</span>}</Translation>
                </NavLink>
            </NavItem>
            </NavList>
        </Nav>
        );
        if(i18next.language === 'fr') return ( 
            <PageSidebar nav={PageNavFR}/>
        ); else return (
            <PageSidebar nav={PageNavEN}/>
        );
    }
}
export default SideNav;