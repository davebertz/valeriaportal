import React from "react";
import ReactDOM from "react-dom";
import "@patternfly/react-core/dist/styles/base.css";
import App from "./App";
import './i18n';


ReactDOM.render(
  <App />,
  document.getElementById("root")
);