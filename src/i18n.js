import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import LanguageDetector from 'i18next-browser-languagedetector';
import Backend from 'i18next-xhr-backend';

i18n
    .use(Backend)
    .use(LanguageDetector)
    .use(initReactI18next)
    .init({
    fallbackLng: "en",
    whitelist: ['en', 'fr'],

    interpolation: {
      escapeValue: false // react already safes from xss
    }
});

export default i18n;